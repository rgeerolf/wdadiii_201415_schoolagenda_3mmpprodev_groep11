#Web Design & Development III Opdracht
##Inhoudstafel
[Inleiding](#inleiding)
[Briefing](#briefing)
[Work Breakdown Structure](#wbs)
[Technische specificaties](#tech_specs)
[Persona’s en scenario’s](#personas)
[Moodboard](#moodboard)
[Sitemap](#sitemap)
[Wireframes](#wireframes)
[Feedback](#feedback)
[Logo](#logo)
[Visual Design](#design)
[Extra Feature](#feature)
##<a id="intro"></a>Inleiding
Voor de eindopdracht van het vak Web Design & Development III is het de bedoeling dat we een schoolagenda webapplicatie bouwen, meer specificaties worden hier verder besproken. Iedereen krijgt dezelfde opdracht.
###Formata
De naam van onze applicatie komt voort van het woord 'Schoolplat **form**'. De term Formata staat voor zorgvuldig en gedisciplineerd boekschrift.

Hieronder kunt u de link vinden naar onze repository.
[Bitbucket Repository](https://bitbucket.org/rgeerolf/wdadiii_201415_schoolagenda_3mmpprodev_groep11)
##<a id="briefing"></a>Briefing
 * Responsive "mobile-first" MVC5+ webapplicatie
 * Schoolplatform/Schoolagenda
 * Persona’s
 
 ###Doel
Er moet een schoolagenda webapplicatie gemaakt worden. Deze moet Responsive zijn, mobile-first en de structuur moet MVC5+ zijn. Er moet rekening gehouden worden met verschillende soorten gebruikers. Iedere soort gebruiker heeft elk zijn eigen functies en mogelijkheden. Van een student die enkel toegang heeft tot de frontoffice en zijn dashboard, tot de directeur die alles kan beheren in de backoffice.
##<a id="wbs"></a>Work Breakdown Structure
Voorbereidingsfase

 * Opstelling van de planning (**1u**)
 * Projectanalyse (**2u**)

  * wat houdt het project in
  * wat zijn de specificaties
  * wat moet bereikt worden
 
 * Bepalen van features (**2u**)
 
  * Welke feature per gebruiker
  * Eventuele eigen extra features bedenken.
 
  Projectplan opstellen 
 * Work Breakdown structure opmaken (**1u30**)
 * Planning afwerken

 Conceptuele fase
 * Moodboard (**1u**)

Ontwerpfase

 * Sitemap (**1u**)
 * Wireframes (+ feedback) (**5u**)
 * Visual Design (**5u**)
 
Data-modeling

 * Welke data is er nodig? (**5u30**)
 * Opstelling EER (**3u**)

Uitwerkingsfase

 * Programmeren Front- & Backoffice (**180u**)

###Conceptuele fase
###Ontwerpfase
###Data-modeling
###Uitwerkingsfase

##<a id="tech_specs"></a>Technische specificaties
 * Responsive "mobile-first" MVC5+ webapplicatie
	* OOP
	*  MVC
		* Viewmodels
	Laag voor de **Models** en **viewmodels** 
	*  Back- & Frontoffice
	* HTML5, CSS3, JavaScript
	Een Framework gebruiken (bv. Ionic) om zo een applicatie te krijgen die aanvoelt als een native applicatie
	* Datalaag voor alle CRUD-operaties
	Er moet ook gebruik gemaakt worden van Unit Testing.
Verder maken we gebruik van het framework Bootstrap, het is makkelijk om hier mee te werken in Visual Studio en we zijn beiden goed bekend met dit framework. Wat betreft framework voor onze mobiele versie hebben we gekozen voor Ionic. Ook hiermee zijn we beiden bekend en de gebruiker ervaart de *look and feel* van een native mobile app.
##<a id="personas"></a>Persona’s en scenario’s
###Arnaud, 15 jaar, leerling
Arnaud is een leerling aan de middelbare school het Atheneum in Mariakerke. Arnaud is een voorbeeldige leerling, behaald goede cijfers en is heel actief bezig met computers en internet. Dankzij het nieuwe platform op school kan Arnaud zijn schoolzaken nu ook bijhouden via het internet en zijn gloednieuwe computer. Hij kan via Formata snel contact hebben met zijn mede klasgenoten en kan vragen stellen die kunnen beantwoord worden door de hele klas.
###Karel, 17 jaar, Leerling
In het schoolgaand leven heeft een laatste jaars student int het 6de middelbaar heel wat kopzorgen, wat zal ik gaan studeren of wat wil ik bereiken met mij leven, en heel wat andere zaken die meer en meer een belangrijke factor spelen in je leven. Karel is een leerling die weet wat hij wil, maar door slordigheid soms vaak zichzelf in de problemen werkt. Dankzij formata kan Karel nu zijn verloren documenten terug vinden. De cursussen kan hij daarvan downloaden en uitprinten alsook taken die moeten gemaakt worden. Verder is karel ook geen goede planner zoals veel leerlingen. Met behulp van Herinnering vergeet Karel zijn taken niet meer in te dienen.
###Marianne, 35 jaar, Ouder van leerling
Marianne is een alleenstaande moeder en heeft nog één grote liefde in haar leven, haar zoontje Lewis. Lewis zit nu in het 2de middelbaar en heeft heel wat problemen met school. Dankzij Formata kan Marianner haar zoontje beter opvolgen. Door vaak zijn kalender te controleren heeft Marianne een beter overzicht wat zoontje lief zijn bezigheid is op school en wanneer hij huiswerk moet maken voor een bepaald vak.
###Bart, 28 jaar , Leerkracht
Bart is een leerkracht aan de middelbare school Sint-Lievens, hij is wiskundeleerkracht en heeft heel wat klassen waar hij les moet aan geven. Uiteraard is zijn passie les geven en moet hij heel wat hand-outs maken voor al zijn leerlingen. Dankzij het nieuwe platform kan Bart al zijn documenten delen met de leerlingen en kunnen ze dit meebrengen tegen de volgende les. Zo heeft Bart zekerheid dat iedereen ze ontvangen heeft, ook in geval van afwezigheid van een leerling. Verder Kan Bart al zijn leerlingen opvolgen en beter beoordelen of helpen.  Het Forum waar hij vragen kan beantwoorden van leerlingen is een gemakkelijk medium voor hem en zo heeft hij meer tijd om zijn lessen rond te krijgen.
###Dirk, 41 jaar  , Schooldirecteur
Dirk is een directeur die graag orde en structuur heeft op zijn school. Tevens heeft dirk ook een passie voor technologie. Formata is voor Dirk een grote database waar hij al zijn leerlingen in kan terugvinden. Hij doet graag aankondigen of zorgt voor tal van evenementen op de school. Dankzij Formata kan hij nu aankondigen sturen naar al zijn leerlingen in enkele seconden. Tevens heeft Dirk een goede kijk op de prestaties van alle leerlingen door de scores van de klassen te bekijken, dit is een verzameling van de rapporten van de studenten.
##<a id="moodboard"></a>Moodboard
![enter image description here](http://s1.postimg.org/bu87npc27/moodboard.jpg)


----------


![enter image description here](http://s30.postimg.org/ufy2ngjv5/moadboard2.png)
##<a id="sitemap"></a>Sitemap
![enter image description here](http://s14.postimg.org/q20o7rx29/Schermafbeelding_2014_11_13_om_08_04_19.png)
##<a id="wireframes"></a>Wireframes
###Home anoniem
####Mobile
![Home Anoniem](http://s8.postimg.org/h6twl4yl1/Home_anonymous.png)


----------


![Home Anoniem Info-menu](http://s8.postimg.org/rfmdqymmt/Home_anonymous_info.png)
###Dashboard (ingelogd)
####Mobile
![Dashboard](http://s8.postimg.org/fgavjnh1x/Home_Dashboard_menu.png)


----------


![Dashboard Menu](http://s8.postimg.org/hm56e5kid/Home_Dashboard.png)
####Desktop
![enter image description here](http://s8.postimg.org/b9q14bhg5/Desktop_HD.png)
##<a id="feedback"></a>Feedback (na Milestone 1 (13/11)
###Positief
* Projectnaam
* Vorm wireframes
###Negatief
* Moodboard te beperkt
* Toelichting Wireframes
* Sitemap nummering
* Persona's uitgebreider beschrijven
* Structuur presentatie
* Gradient in wireframes
##<a id="logo"></a>Logo
Het logo is simpel en duidelijk. Het woord Formata is in hetzelfde lettertype als in de webapp en op het woord is een afstudeerhoed te vinden.
![enter image description here](http://s2.postimg.org/rpdpz9jcp/logo.png)

##<a id="design"></a>Visual Design
###Mobiel
####Frontoffice
![enter image description here](http://s15.postimg.org/f3n7c7sm3/Home_anonymous.png)


----------


![enter image description here](http://s15.postimg.org/qu14tllej/Home_anonymous_info.png)
####Dashboard
![enter image description here](http://s15.postimg.org/kds63icuz/Home_Dashboard.png)
  


----------


![enter image description here](http://s15.postimg.org/bu8s5r4ij/Home_Dashboard_menu.png)
###Desktop
####Dashboard
![enter image description here](http://s18.postimg.org/713ht62e1/Desktop_HD_Dashboard.png)
#####Messages
![enter image description here](http://s17.postimg.org/897autijz/Home_Dashboard_messages.png)
####Backoffice
![enter image description here](http://s18.postimg.org/jr7q69ac9/Desktop_HD_admin.png)


##<a id="data"></a>Data Modelling
![enter image description here](https://bytebucket.org/rgeerolf/wdadiii_201415_schoolagenda_3mmpprodev_groep11/raw/c6feb0d9fbbf2c8eaf8b3db412a6b008f6f88d8b/dbScheme/formata.jpg)

##<a id="feature"></a>Extra feature
Een van onze extra features, dus feature die we zouden inbouwen als we genoeg tijd over hebben, is het uploaden/downloaden en dus delen van bestanden. Zo kan een leerkracht zijn cursus delen met zijn studenten via ons platform.


----------
Ben Boute
Rutger Geerolf
3MMP proDEV