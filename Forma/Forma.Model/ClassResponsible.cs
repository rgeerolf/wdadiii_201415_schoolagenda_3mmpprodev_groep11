﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class ClassResponsible
    {
        #region Properties
        public Int16 Id { get; set; }
        public string ResponsibleNumber { get; set; }

        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public int LecturerId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual Lecturer Lecturer { get; set; }
        public virtual ICollection<Classes> classes { get; set; }
        #endregion
    }
}

