﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>
    {
        #region Extra Properties
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion
    }
}
