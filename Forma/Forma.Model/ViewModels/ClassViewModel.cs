﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forma.Model.ViewModels
{
    public class ClassViewModel
    {
        public Classes Classes { get; set; }
        public IEnumerable<Specialization> Specialization { get; set; }

        public IEnumerable<ClassResponsible> Responsible { get; set; }

        //public IEnumerable<Courses> Courses { get; set; }
        public int[] CoursesIds { get; set; }

        public IEnumerable<SelectListItem> Courses { get; set; }

    }
}
