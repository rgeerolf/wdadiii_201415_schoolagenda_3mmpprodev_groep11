﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.Viewmodels
{
    public class SpecializationViewModel
    {
        public Specialization Specialization { get; set; }
        public IEnumerable<Department> Departments { get; set; }
    }
}
