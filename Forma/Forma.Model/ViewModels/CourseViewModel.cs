﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.ViewModels
{
    public class CourseViewModel
    {
        public Courses courses { get; set; }

        public virtual IEnumerable<Lecturer> lecturers { get; set; }
    }
}
