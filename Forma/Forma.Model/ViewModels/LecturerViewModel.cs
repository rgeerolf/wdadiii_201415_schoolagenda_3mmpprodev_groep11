﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.ViewModels
{
    public class LecturerViewModel
    {
        public Lecturer lecturer { get; set; }
        public IEnumerable<ApplicationUser> users { get; set; }

    }
}
