﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.Viewmodels
{
    public class DepartmentViewModel
    {
        public Department Department { get; set; }
        public IEnumerable<AcademicYear> AcademicYears { get; set; }
    }
}
