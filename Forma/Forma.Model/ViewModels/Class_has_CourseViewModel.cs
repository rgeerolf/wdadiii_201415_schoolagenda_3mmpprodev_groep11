﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.ViewModels
{
    public class Class_has_CourseViewModel
    {
        public Classes Classes { get; set; }
        public Courses Courses { get; set; }
  
        public IEnumerable<Classes> classes { get; set; }
        public IEnumerable<Courses> courses { get; set; }

    }
}
