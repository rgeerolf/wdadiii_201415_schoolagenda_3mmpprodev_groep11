﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.ViewModels
{
    public class StudentViewModel
    {
        public Student student { get; set; }

        public IEnumerable<ApplicationUser> users { get; set; }
        public IEnumerable<Classes> classes { get; set; }
    }
}
