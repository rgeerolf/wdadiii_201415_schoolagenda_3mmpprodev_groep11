﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model.Viewmodels
{
    public class UserRegisterViewModel
    {
        
        [StringLength(12, MinimumLength = 6, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        //[Required]
        //[StringLength(12, MinimumLength = 6, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and the confirmation password do not match!")]
        public string ConfirmPassword { get; set; }
        public Person Person { get; set; }

    }
}
