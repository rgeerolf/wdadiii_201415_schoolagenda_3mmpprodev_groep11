﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
   public class Student
    {

        #region ExtraProperties
       public Int16 Id { get; set; }

       public string StudentNumber { get; set; }
       public string StudentEmail { get; set; }
       public string StudentPicture { get; set; }
       public Byte[] Created { get; set; }
       public DateTime? Updated { get; set; }
       public DateTime? Deleted { get; set; }
        #endregion

       #region Foreign Keys
       public int UserId { get; set; }
       public Int16 ClassId { get; set; }
       #endregion

       #region Navigation Properties
       public virtual ApplicationUser User { get; set; }
       public virtual Classes Classes { get; set; }

       #endregion
    }
}
