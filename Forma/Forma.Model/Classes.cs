﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class Classes
    {
        #region properties

        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region foreign keys

        public Int16 SpecializationId { get; set; }
        public Int16 ResponsibleId { get; set; }
       
        #endregion

        #region Navigation Properties
        public virtual Specialization Specialization { get; set; }
        public virtual ClassResponsible Responsible { get; set; }
        public virtual ICollection<Courses> Courses { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        



        #endregion
    }
}
