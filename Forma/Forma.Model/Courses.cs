﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class Courses
    {
        #region properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region foreign keys
        public int LecturerId { get; set; }

        #endregion

        #region Navigation Properties
        public virtual ICollection<Classes> classes { get; set; }
        public virtual Lecturer lecturer { get; set; }

        #endregion
    }
}
