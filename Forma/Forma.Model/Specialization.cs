﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
   public class Specialization
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
         public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion
       


        #region Foreign Keys
        public Int16 DepartmentId { get; set; }
        #endregion

        #region Navigation Properties
         public virtual Department Department { get; set; }
         public virtual ICollection<Classes> classes {get; set;}
        
        #endregion

    }
}
