﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class Department
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 AcademicYearId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual ICollection<Specialization> Specialization { get; set; }
        //public virtual ICollection<Post> Posts { get; set; }
        #endregion
    }
}
