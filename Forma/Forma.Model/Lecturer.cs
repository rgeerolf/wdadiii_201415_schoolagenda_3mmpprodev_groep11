﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma.Model
{
    public class Lecturer
    {
        #region Properties
        public int Id { get; set; }
        public string LecturerNumber { get; set; }
        public string LecturerEmail { get; set; }
        public string LecturerPicture { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public int UserId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<Courses> Courses { get; set; }
        #endregion
    }
}
