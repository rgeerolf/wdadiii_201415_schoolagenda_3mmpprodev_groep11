﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Forma.Model
{
    public class AcademicYear
    {
        #region Properties
        public Int16 Id { get; set; }
        [Required]
        [StringLength(12, MinimumLength = 4, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<Department> Departments { get; set; }
        #endregion
    }
}
