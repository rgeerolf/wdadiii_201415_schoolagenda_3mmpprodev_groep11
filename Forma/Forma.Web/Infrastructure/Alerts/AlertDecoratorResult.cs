﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forma.Web.Infrastructure.Alerts

{
    public class AlertDecoratorResult : ActionResult
    {
        public ActionResult InnerResult { get; set; }
        public AlertType Type { get; set; }
        public string Message { get; set; }

        public AlertDecoratorResult(ActionResult innerResult, AlertType type, string message)
        {
            InnerResult = innerResult;
            Type = type;
            Message = message;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var alerts = context.Controller.TempData.GetAlerts();
            alerts.Add(new Alert(Type, Message));
            InnerResult.ExecuteResult(context);
        }
    }
}