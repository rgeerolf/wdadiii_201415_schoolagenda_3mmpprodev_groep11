﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forma.Web.Infrastructure.Alerts

{
    public enum AlertType
    {
        Error,
        Info,
        Warning,
        Success
    }

    public class Alert
    {
        public AlertType Type { get; set; }
        public string Message { get; set; }

        public Alert(AlertType type, string message)
        {
            Type = type;
            Message = message;
        }
    }
}