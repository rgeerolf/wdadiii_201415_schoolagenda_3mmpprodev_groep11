﻿using Data.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Data.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;

namespace Forma.Web.Controllers
{
    public class CommonController : Controller
    {

        #region Variables
        private GenericUnitOfWork _unitOfWork;
        private ApplicationUserManager _applicationUserManager;
        private ApplicationRoleManager _applicationRoleManager;
        #endregion

        #region Properties
        protected GenericUnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                {
                    _unitOfWork = new GenericUnitOfWork(new ApplicationDbContext());
                }
                return _unitOfWork;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _applicationUserManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
            protected set
            {
                _applicationUserManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _applicationRoleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            protected set
            {
                _applicationRoleManager = value;
            }
        }
        #endregion
    }
}
