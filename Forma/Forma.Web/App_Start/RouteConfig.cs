﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Forma.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           // routes.MapRoute(
           //    name: "academicyear",
           //    url: "Backoffice/Academicyear/{action}/{id}",
           //    defaults: new { controller = "AcademicYear" },
           //    constraints: new { action = "create|index|edit|delete" }
           //);

            routes.MapRoute(
                name:"AcademicYear",
                url: "Backoffice/AcademicYear/Delete/3",
                defaults: new { controller = "AcademicYear"},
                constraints: new { action = "Delete"}
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Forma.Web.Controllers" }).DataTokens["UseNamespaceFallback"] = false;
           
        }
    }
}
