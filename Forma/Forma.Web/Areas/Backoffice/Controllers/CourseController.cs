﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.ViewModels;


namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class CourseController : CommonController
    {
        // GET: Backoffice/Lecturer

        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Courses>().GetAll().OrderByDescending(m => m.Created).ToArray();

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        // GET: Backoffice/Lecturer/Create
        public ActionResult Create()
        {
            var model = new CourseViewModel

            {
                courses = new Courses(),
                lecturers = UnitOfWork.Repository<Lecturer>().GetAll().ToArray()
            };
            return View(model);
        }
        // POST: Backoffice/Lecturer/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CourseViewModel model)
        {
             try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Course is not valid!");

                var courses = model.courses;


                UnitOfWork.Repository<Courses>().Insert(courses);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new CourseViewModel
                {
                    courses = model.courses,
                    lecturers = UnitOfWork.Repository<Lecturer>().GetAll()
                };
                return View(viewModel);
            }
           
        }

   }
}