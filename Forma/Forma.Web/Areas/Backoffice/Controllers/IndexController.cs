﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forma.Web.Areas.Backoffice.Controllers

{
    public class IndexController : CommonController
    {
        // GET: Backoffice/Index
        public ActionResult Index()
        {
            return View();
        }
        //Get: Backoffice/Home
        public ActionResult Home()
        {
            return View();
        }
    }
}