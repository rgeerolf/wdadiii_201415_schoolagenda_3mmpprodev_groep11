﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.ViewModels;


namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class LecturerController : CommonController
    {
        // GET: Backoffice/Lecturer
       
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Lecturer>().GetAll().OrderByDescending(m => m.Created).ToArray();
            
            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        // GET: Backoffice/Lecturer/Create
        public ActionResult Create()
        {
            var model =  new LecturerViewModel

            {
                lecturer = new Lecturer(),
                users = UnitOfWork.Repository<ApplicationUser>().GetAll().ToArray()
            };
            return View(model);
        }
        // POST: Backoffice/Lecturer/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(LecturerViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Lecturer is not valid!");

             
                
                //Random rnd = new Random();
                //var getal1 = rnd.Next(100);
                //var getal2 = rnd.Next(100);

                
               // string lecturerNumber =+getal2;
                var lecturer = new Lecturer{ LecturerNumber = "nummer", LecturerEmail = model.lecturer.LecturerEmail, UserId = model.lecturer.UserId };

                //var person = new Person { FirstName = model.Person.FirstName, LastName = model.Person.LastName };
               

                UnitOfWork.Repository<Lecturer>().Insert(lecturer);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new LecturerViewModel
                {
                    lecturer = model.lecturer,
                    users = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };
                return View(viewModel);
            }
        }

    }
}