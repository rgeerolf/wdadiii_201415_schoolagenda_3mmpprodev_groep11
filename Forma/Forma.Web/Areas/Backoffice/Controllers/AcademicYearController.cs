﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.Viewmodels;

 namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class AcademicYearController : CommonController
    {
        // GET: Backoffice/AcademicYear
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<AcademicYear>().GetAll();
           model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/AcademicYear/Create
        public ActionResult Create()
        {
            var model = new AcademicYear();//Create empty model

            return View(model);
        }

        // POST: Backoffice/AcademicYear/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(AcademicYear model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("AcademicYear is not valid!");


                UnitOfWork.Repository<AcademicYear>().Insert(model);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception )
            {
                return View(model);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get a specific AcademicYear By Id (primary key)
            var model = UnitOfWork.Repository<AcademicYear>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(AcademicYear model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("AcademicYear is not valid!");

                var originalModel = UnitOfWork.Repository<AcademicYear>().GetById(model.Id);

                if (originalModel == null)
                    throw new Exception("AcademicYear do not exist!");

                originalModel.Name = model.Name;
                originalModel.Description = model.Description;

                UnitOfWork.Repository<AcademicYear>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }
        // POST: Backoffice/AcademicYear/Delete/id
        [ValidateAntiForgeryToken]
        //[HttpPost]
        public ActionResult Delete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<AcademicYear>().GetById(id);

                if (model == null)
                    throw new Exception("AcademicYear does not exist!");

                UnitOfWork.Repository<AcademicYear>().Delete(id);
                var result = UnitOfWork.SaveChanges();



                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception)
            {


                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }


    }
}