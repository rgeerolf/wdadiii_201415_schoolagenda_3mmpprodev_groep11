﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.Viewmodels;
//using Forma.Web.Infrastructure.Alerts;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class UserController : CommonController
    {
        // GET: Backoffice/User
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetAll();
          model = model.OrderByDescending(m => m.UserName);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new UserRegisterViewModel();

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Create(UserRegisterViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("User is not valid");

                var firstname = model.Person.FirstName;
                var lastname = model.Person.LastName;
                var username = firstname.Substring(0, 4) + lastname.Substring(0, 4);
                
                

               

                var person = new Person { FirstName = model.Person.FirstName, LastName = model.Person.LastName };
                UnitOfWork.Repository<Person>().Insert(person);
                int personresult = UnitOfWork.SaveChanges();


                //Create a new ApplicationUser
                var user = new ApplicationUser { UserName = username, Email = model.Email , PersonId = person.Id };
                //Create the user via the UserManager
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    AddErrors(result);
                    throw new Exception("Can't create the user!");
                }

            }
            catch (Exception)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().Delete(id);
                int result = UnitOfWork.SaveChanges();

               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception)
            {
              

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception)
            {
                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception)
            {
               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Lock(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                var now = DateTime.Now;
                now = now.AddMonths(1);

                if (!UserManager.GetLockoutEnabled(id))
                    await UserManager.SetLockoutEnabledAsync(id, true);

                var result = await UserManager.SetLockoutEndDateAsync(id, new DateTimeOffset(now));

                if (result.Succeeded)
                {
                    

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = "Locked" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    throw new Exception("Can't lock the user!");
                }

            }
            catch (Exception )
            {
                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> UnLock(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                if (!UserManager.GetLockoutEnabled(id))
                    throw new Exception("User can't be unlocked!");

                var result = await UserManager.SetLockoutEndDateAsync(id, DateTimeOffset.MinValue);

                if (result.Succeeded)
                {
                    

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = "unlocked" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    throw new Exception("Can't unlock the user!");
                }

            }
            catch (Exception)
            {
                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}