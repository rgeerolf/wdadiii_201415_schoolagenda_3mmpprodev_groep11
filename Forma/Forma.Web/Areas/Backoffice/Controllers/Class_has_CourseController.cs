﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Model.ViewModels;
using Forma.Model;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class Class_has_CourseController : CommonController
    {
        // GET: Backoffice/Class_has_Course
        public ActionResult Index()
        {
           

            var model = new Class_has_CourseViewModel

            { 
                classes = UnitOfWork.Repository<Classes>().GetAll().ToArray(),
                courses = UnitOfWork.Repository<Courses>().GetAll().ToArray()

            };


            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(Class_has_CourseViewModel model) {

            var Class_has_CourseModel = new Class_has_CourseViewModel

            {
                classes = UnitOfWork.Repository<Classes>().GetAll().ToArray(),
                courses = UnitOfWork.Repository<Courses>().GetAll().ToArray()
            };
            return View(Class_has_CourseModel);
        }
    }
}