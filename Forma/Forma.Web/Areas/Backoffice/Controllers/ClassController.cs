﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Web;
using Forma.Model.ViewModels;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class ClassController : CommonController
    {
        // GET: Backoffice/Class
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Classes>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Classes, object>>> { x => x.Specialization });
            model = model.OrderByDescending(m => m.Id);
                        
            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        // GET: Backoffice/Class/Create
        public ActionResult Create()
        {
            var model = new ClassViewModel

            {
                Classes = new Classes(),
                Specialization = UnitOfWork.Repository<Specialization>().GetAll(),
                Responsible = UnitOfWork.Repository<ClassResponsible>().GetAll(),
                Courses = new MultiSelectList(UnitOfWork.Repository<Courses>().GetAll(), "Id", "Name")
              
            };
            return View(model);
        }

        // POST: Backoffice/Class/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ClassViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Class is not valid!");

                var classes = model.Classes;
                var ids = model.CoursesIds;
                if (ids != null && ids.Length > 0)
                {
                    var courses = new List<Courses>();
                    foreach (var id in ids)
                    {
                        var Course = UnitOfWork.Repository<Courses>().GetById(id);
                        courses.Add(Course);
                    }
                    classes.Courses = courses;
                }

                UnitOfWork.Repository<Classes>().Insert(classes);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new ClassViewModel
                {
                    Classes = model.Classes,
                    Specialization = UnitOfWork.Repository<Specialization>().GetAll()
                };
                return View(viewModel);
            }
        }

        // GET: Backoffice/Class/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Backoffice/Class/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Backoffice/Class/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Backoffice/Class/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
