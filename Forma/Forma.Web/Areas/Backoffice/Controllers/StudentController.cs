﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.ViewModels;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class StudentController : CommonController
    {
        // GET: Backoffice/Student
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Student>().GetAll().OrderByDescending(m => m.Created).ToArray();

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        public ActionResult Create()
        {
            var model = new StudentViewModel

            {
                student = new Student(),
                users = UnitOfWork.Repository<ApplicationUser>().GetAll().ToArray(),
                classes = UnitOfWork.Repository<Classes>().GetAll().ToArray()
            };
            return View(model);
        }
        // POST: Backoffice/Student/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(StudentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Student is not valid!");


                //var firstname = model.student.User.Person.FirstName;
                //var lastname = model.student.User.Person.LastName;
                //Random rnd = new Random();
                //var getal1 = rnd.Next(100);
                //var getal2 = rnd.Next(100);
                //var getal3 = rnd.Next(100);
                //var getal4 = rnd.Next(100);


                var number = "nummer";
                var student = new Student { StudentNumber = number, StudentEmail = model.student.StudentEmail, UserId = model.student.UserId, ClassId = model.student.ClassId };

                


                UnitOfWork.Repository<Student>().Insert(student);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new StudentViewModel
                {
                    student = model.student,
                    users = UnitOfWork.Repository<ApplicationUser>().GetAll(),
                    classes = UnitOfWork.Repository<Classes>().GetAll()
                };
                return View(viewModel);
            }
        }
    }
}