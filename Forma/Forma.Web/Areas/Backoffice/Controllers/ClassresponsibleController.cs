﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.ViewModels;


namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class ClassresponsibleController : CommonController
    {
        // GET: Backoffice/Classresponsible
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ClassResponsible>().GetAll().OrderByDescending(m => m.Created).ToArray();

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        // GET: Backoffice/Classresponsible/Create
        public ActionResult Create()
        {
            var model = new ResponsibleViewModel

            {
                Classresponsible = new ClassResponsible(),
                Lecturers = UnitOfWork.Repository<Lecturer>().GetAll().ToArray()
            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ResponsibleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Lecturer is not valid!");



                //Random rnd = new Random();
                //var getal1 = rnd.Next(100);
                //var getal2 = rnd.Next(100);


                // string lecturerNumber = lecturerNumber + getal2.ToString();
                var Responsible = new ClassResponsible { ResponsibleNumber = "nummer",  LecturerId = model.Classresponsible.LecturerId };

                //var person = new Person { FirstName = model.Person.FirstName, LastName = model.Person.LastName };


                UnitOfWork.Repository<ClassResponsible>().Insert(Responsible);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new ResponsibleViewModel
                {
                    Classresponsible = model.Classresponsible,
                    Lecturers = UnitOfWork.Repository<Lecturer>().GetAll()
                };
                return View(viewModel);
            }
        }
    }
}