﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Web;
using Forma.Model.Viewmodels;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class SpecializationController : CommonController
    {
        // GET: Backoffice/Specialization
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Specialization>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Specialization, object>>> { x => x.Department });
            model = model.OrderByDescending(m => m.Id);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Specialization/Create
        public ActionResult Create()
        {
            var model = new SpecializationViewModel

            {
                Specialization = new Specialization(),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(SpecializationViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Specialization is not valid!");

                var Specialization = model.Specialization;

                UnitOfWork.Repository<Specialization>().Insert(Specialization);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new SpecializationViewModel
                {
                    Specialization = model.Specialization,
                    Departments = UnitOfWork.Repository<Department>().GetAll()
                };
                return View(viewModel);
            }
        }

        // GET: Backoffice/Specialization/Edit/5
        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new SpecializationViewModel
            {
                Specialization = UnitOfWork.Repository<Specialization>().GetById(id),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // POST: Backoffice/Specialization/Edit/5
        [HttpPost]
        public ActionResult Edit(SpecializationViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Specialization is not valid!");

                var originalModel = UnitOfWork.Repository<Specialization>().GetById(model.Specialization.Id);

                if (originalModel == null)
                    throw new Exception("Department do not exist!");

                originalModel.Name = model.Specialization.Name;
                originalModel.Description = model.Specialization.Description;
                originalModel.DepartmentId = model.Specialization.DepartmentId;
                //originalModel.AcademicYear = model.Department.AcademicYear;

                UnitOfWork.Repository<Specialization>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(Int16 id)
        {
           
            try
            {
                var model = UnitOfWork.Repository<Specialization>().GetById(id);

                if (model == null)
                    throw new Exception("Specialization does not exist!");

                UnitOfWork.Repository<Specialization>().Delete(id);
                var result = UnitOfWork.SaveChanges();

                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index","Specialization");
                }

            }
            catch (Exception)
            {
                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index","Specialization");
                }
            }
        
        } 
    }
}
