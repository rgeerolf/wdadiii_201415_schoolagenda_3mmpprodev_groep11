﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forma.Model;
using Forma.Model.Viewmodels;

namespace Forma.Web.Areas.Backoffice.Controllers
{
    public class DepartmentController : CommonController
    {
        // GET: Backoffice/Department
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Department>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Department, object>>> { x => x.AcademicYear });
            model = model.OrderByDescending(m => m.Id);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }
        // GET: Backoffice/Department/Create
        public ActionResult Create()
        {
            var model = new DepartmentViewModel

            {
                Department = new Department(),
                AcademicYears = UnitOfWork.Repository<AcademicYear>().GetAll()
            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(DepartmentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Department is not valid!");

                var department = model.Department;

                UnitOfWork.Repository<Department>().Insert(department);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                var viewModel = new DepartmentViewModel
                {
                    Department = model.Department,
                    AcademicYears = UnitOfWork.Repository<AcademicYear>().GetAll()
                };
                return View(viewModel);
            }
        }
        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new DepartmentViewModel
            {
                Department = UnitOfWork.Repository<Department>().GetById(id),
                AcademicYears = UnitOfWork.Repository<AcademicYear>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(DepartmentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Department is not valid!");

                var originalModel = UnitOfWork.Repository<Department>().GetById(model.Department.Id);

                if (originalModel == null)
                    throw new Exception("Department do not exist!");

                originalModel.Name = model.Department.Name;
                originalModel.Description = model.Department.Description;
                originalModel.AcademicYearId = model.Department.AcademicYearId;
                //originalModel.AcademicYear = model.Department.AcademicYear;

                UnitOfWork.Repository<Department>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().Delete(id);
                var result = UnitOfWork.SaveChanges();

                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception)
            {
                

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

              

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception)
            {
               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = "Done" });
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception)
            {
               

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = "Error" });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
    }
}