﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forma.Web.Areas.Dashboard.Controllers
{
    public class IndexController : DefaultController //gebruik van defaultController (UnitOFWork)
    {
        // GET: Dashboard/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}