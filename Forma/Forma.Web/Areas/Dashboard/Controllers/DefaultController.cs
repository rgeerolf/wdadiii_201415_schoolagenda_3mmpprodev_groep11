﻿using System;
using Data.Pattern.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forma.Data.Persistence;

namespace Forma.Web.Areas.Dashboard.Controllers
{
    public class DefaultController : Controller
    {
        #region Variables
        private GenericUnitOfWork _unitOfWork;
        #endregion

        #region Properties
        protected GenericUnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                {
                    _unitOfWork = new GenericUnitOfWork(new ApplicationDbContext());
                }
                return _unitOfWork;
            }
        }
        #endregion
    }
}