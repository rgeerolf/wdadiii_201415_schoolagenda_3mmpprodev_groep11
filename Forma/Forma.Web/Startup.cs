﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Forma.Web.Startup))]
namespace Forma.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
