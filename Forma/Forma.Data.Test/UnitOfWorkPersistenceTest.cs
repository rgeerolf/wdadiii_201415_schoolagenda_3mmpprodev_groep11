﻿using Data.Pattern.UnitOfWork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forma.Data.Persistence;
using Forma.Model;

namespace Forma.Data.Test
{
    [TestClass]
    public class UnitOfWorkPersitenceTest
    {
        #region Variables
        GenericUnitOfWork _unitOfWork = new GenericUnitOfWork(new ApplicationDbContext());
        #endregion



       [TestMethod]
        public void UOW_Create_AcademicYear()
        {

            var academicyear = new AcademicYear
            {
                Name = "2014-15",
                Description = "Academic year 2014-15",
                StartDate = DateTime.Now,
                
               
                
                
            };

            _unitOfWork.Repository<AcademicYear>().Insert(academicyear);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Department()
        {
            var academicYear = _unitOfWork.Repository<AcademicYear>().GetById(1);

            Assert.IsNotNull(academicYear);

            var department = new Department
            {
                Name = "Bachelor in de grafische en digitale media",
                Description = "Kun jij je geen leven zonder het web voorstellen? Kan je niet door een krant of tijdschrift bladeren zonder te focussen op lay-out en vormgeving? Of zie je jezelf als marketingstrateeg de crossmediaconcepten van de toekomst bedenken? Een unieke opleiding vol projecten en contacten met de bedrijfswereld staat klaar om van jou een gedreven professional te maken.",
               
            };

            _unitOfWork.Repository<Department>().Insert(department);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
    }
}
