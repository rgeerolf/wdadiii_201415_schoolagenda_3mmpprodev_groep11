﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forma.Data.Initializer;
using Forma.Model;

namespace Forma.Data.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        #region Properties
        
        #endregion

        #region Constructor
        public ApplicationDbContext() : base("DefaultConnection") { }
        static ApplicationDbContext()
        {
            //Seeds the database
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
                throw new ArgumentNullException("modelBuilder");

            base.OnModelCreating(modelBuilder);

            

            //Map AcademicYear Model to AcademicYears Table
            EntityTypeConfiguration<AcademicYear> efConfAcademicYear = modelBuilder.Entity<AcademicYear>().ToTable("AcademicYears");
            efConfAcademicYear.HasKey((AcademicYear m) => m.Id);//Linq notation
            efConfAcademicYear.Property((AcademicYear m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfAcademicYear.Property((AcademicYear m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfAcademicYear.Property((AcademicYear m) => m.Name).IsRequired().HasMaxLength(128);
            efConfAcademicYear.Property((AcademicYear m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfAcademicYear.Property((AcademicYear m) => m.Updated).IsOptional();
            efConfAcademicYear.Property((AcademicYear m) => m.Deleted).IsOptional();
            efConfAcademicYear.Property((AcademicYear m) => m.StartDate).IsOptional();
            efConfAcademicYear.Property((AcademicYear m) => m.EndDate).IsOptional();
           
            #region Department
            //Map Department Model to Departments Table
            EntityTypeConfiguration<Department> efConfDepartment = modelBuilder.Entity<Department>().ToTable("Departments");
            efConfDepartment.HasKey((Department m) => m.Id);//Linq notation
            efConfDepartment.Property((Department m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfDepartment.Property((Department m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfDepartment.Property((Department m) => m.Name).IsRequired().HasMaxLength(128);
            efConfDepartment.Property((Department m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfDepartment.Property((Department m) => m.Updated).IsOptional();
            efConfDepartment.Property((Department m) => m.Deleted).IsOptional();
            efConfDepartment.HasRequired((Department m) => m.AcademicYear).WithMany((AcademicYear a) => a.Departments).HasForeignKey((Department f) => f.AcademicYearId);
            #endregion
            #region Specialization
            //Map Specialization Model to Specializations Table
            EntityTypeConfiguration<Specialization> efConfSpecialization = modelBuilder.Entity<Specialization>().ToTable("Specializations");
            efConfSpecialization.HasKey((Specialization m) => m.Id);//Linq notation
            efConfSpecialization.Property((Specialization m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfSpecialization.Property((Specialization m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfSpecialization.Property((Specialization m) => m.Name).IsRequired().HasMaxLength(128);
            efConfSpecialization.Property((Specialization m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfSpecialization.Property((Specialization m) => m.Updated).IsOptional();
            efConfSpecialization.Property((Specialization m) => m.Deleted).IsOptional();
            efConfSpecialization.HasRequired((Specialization m) => m.Department).WithMany((Department a) => a.Specialization).HasForeignKey((Specialization f) => f.DepartmentId);
            #endregion


            //Map Classes Model to Classes Table
            EntityTypeConfiguration<Classes> efConfClasses = modelBuilder.Entity<Classes>().ToTable("Classes");
            efConfClasses.HasKey((Classes m) => m.Id);//Linq notation
            efConfClasses.Property((Classes m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfClasses.Property((Classes m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfClasses.Property((Classes m) => m.Name).IsRequired().HasMaxLength(128);
            efConfClasses.Property((Classes m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfClasses.Property((Classes m) => m.Updated).IsOptional();
            efConfClasses.Property((Classes m) => m.Deleted).IsOptional();
            efConfClasses.HasRequired((Classes m) => m.Specialization).WithMany((Specialization a) => a.classes).HasForeignKey((Classes f) => f.SpecializationId);
            efConfClasses.HasRequired((Classes m) => m.Responsible).WithMany((ClassResponsible a) => a.classes).HasForeignKey((Classes f) => f.ResponsibleId);
            efConfClasses
               .HasMany((Classes m) => m.Courses)
               .WithMany((Courses c) => c.classes)
               .Map(mc =>
               {
                   mc.ToTable("Classes_has_Courses");
                   mc.MapLeftKey("ClassesId");
                   mc.MapRightKey("CoursesId");
               });

            //Map Courses Model to Courses Table + many to many on classes
            EntityTypeConfiguration<Courses> efConfCourses = modelBuilder.Entity<Courses>().ToTable("Courses");
            efConfCourses.HasKey((Courses m) => m.Id);//Linq notation
            efConfCourses.Property((Courses m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfCourses.Property((Courses m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfCourses.Property((Courses m) => m.Name).IsRequired().HasMaxLength(128);
            efConfCourses.Property((Courses m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfCourses.Property((Courses m) => m.Updated).IsOptional();
            efConfCourses.Property((Courses m) => m.Deleted).IsOptional();


            // Map Person Model to the Persons Table
            EntityTypeConfiguration<Person> efConfPerson = modelBuilder.Entity<Person>().ToTable("Persons");
            efConfPerson.HasKey((Person m) => m.Id);
            efConfPerson.Property((Person m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfPerson.Property((Person m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfPerson.Property((Person m) => m.FirstName).IsRequired().HasMaxLength(64);
            efConfPerson.Property((Person m) => m.LastName).IsRequired().HasMaxLength(128);
            efConfPerson.Property((Person m) => m.DayOfBirth).IsOptional();
            efConfPerson.Property((Person m) => m.Email).IsOptional().HasMaxLength(256);
            modelBuilder.Entity<ApplicationUser>().HasOptional((ApplicationUser u) => u.Person).WithMany().HasForeignKey((ApplicationUser u) => u.PersonId);// Simulate zero-to-zero

            // Map Lecturer Model to the Lecturers Table
            modelBuilder.Entity<Lecturer>().ToTable("Lecturers");
           // Classresponsible
            modelBuilder.Entity<ClassResponsible>().ToTable("ClassResponsibles");
            // Student

            //Map Classes Model to Classes Table
            EntityTypeConfiguration<Student> efConfStudent = modelBuilder.Entity<Student>().ToTable("Students");
            efConfStudent.HasKey((Student m) => m.Id);//Linq notation
            efConfStudent.Property((Student m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfStudent.Property((Student m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfStudent.Property((Student m) => m.StudentNumber).IsRequired().HasMaxLength(10);
            efConfStudent.Property((Student m) => m.StudentEmail).IsRequired().HasMaxLength(256);
            efConfStudent.Property((Student m) => m.StudentPicture).IsOptional();
            efConfStudent.Property((Student m) => m.Deleted).IsOptional();
            efConfStudent.HasRequired((Student m) => m.Classes).WithMany((Classes a) => a.Students).HasForeignKey((Student f) => f.ClassId);
          
            
        }

        #endregion

        #region Public Methods
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
        #endregion

    }
}
