namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CourseStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lecturers", "Courses_Id", "dbo.Courses");
            DropIndex("dbo.Lecturers", new[] { "Courses_Id" });
           // DropColumn("dbo.Courses", "LecturerId");
           // RenameColumn(table: "dbo.Courses", name: "Courses_Id", newName: "LecturerId");
            CreateIndex("dbo.Courses", "LecturerId");
            AddForeignKey("dbo.Courses", "LecturerId", "dbo.Lecturers", "Id", cascadeDelete: true);
            //DropColumn("dbo.Lecturers", "Courses_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lecturers", "Courses_Id", c => c.Short());
            DropForeignKey("dbo.Courses", "LecturerId", "dbo.Lecturers");
            DropIndex("dbo.Courses", new[] { "LecturerId" });
            RenameColumn(table: "dbo.Courses", name: "LecturerId", newName: "Courses_Id");
            AddColumn("dbo.Courses", "LecturerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Lecturers", "Courses_Id");
            AddForeignKey("dbo.Lecturers", "Courses_Id", "dbo.Courses", "Id");
        }
    }
}
