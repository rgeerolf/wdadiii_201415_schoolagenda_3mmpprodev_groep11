namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class startStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AcademicYears",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        AcademicYearId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AcademicYears", t => t.AcademicYearId, cascadeDelete: true)
                .Index(t => t.AcademicYearId);
            
            CreateTable(
                "dbo.Specializations",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        DepartmentId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        SpecializationId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Specializations", t => t.SpecializationId, cascadeDelete: true)
                .Index(t => t.SpecializationId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Classes_has_Courses",
                c => new
                    {
                        ClassesId = c.Short(nullable: false),
                        CoursesId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClassesId, t.CoursesId })
                .ForeignKey("dbo.Classes", t => t.ClassesId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CoursesId, cascadeDelete: true)
                .Index(t => t.ClassesId)
                .Index(t => t.CoursesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Specializations", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Classes", "SpecializationId", "dbo.Specializations");
            DropForeignKey("dbo.Classes_has_Courses", "CoursesId", "dbo.Courses");
            DropForeignKey("dbo.Classes_has_Courses", "ClassesId", "dbo.Classes");
            DropForeignKey("dbo.Departments", "AcademicYearId", "dbo.AcademicYears");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.Classes_has_Courses", new[] { "CoursesId" });
            DropIndex("dbo.Classes_has_Courses", new[] { "ClassesId" });
            DropIndex("dbo.Classes", new[] { "SpecializationId" });
            DropIndex("dbo.Specializations", new[] { "DepartmentId" });
            DropIndex("dbo.Departments", new[] { "AcademicYearId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.Classes_has_Courses");
            DropTable("dbo.Courses");
            DropTable("dbo.Classes");
            DropTable("dbo.Specializations");
            DropTable("dbo.Departments");
            DropTable("dbo.AcademicYears");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
        }
    }
}
