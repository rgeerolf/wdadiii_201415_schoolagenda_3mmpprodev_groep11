namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Student_has_ClasStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Students", "Id", "dbo.Persons");
            DropIndex("dbo.Students", new[] { "Id" });
            DropPrimaryKey("dbo.Students");
            AddColumn("dbo.Students", "Created", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Students", "Updated", c => c.DateTime());
            AddColumn("dbo.Students", "Deleted", c => c.DateTime());
            AddColumn("dbo.Students", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Students", "ClassId", c => c.Short(nullable: false));
            AlterColumn("dbo.Students", "Id", c => c.Short(nullable: false, identity: true));
            AlterColumn("dbo.Students", "StudentNumber", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Students", "StudentEmail", c => c.String(nullable: false, maxLength: 256));
            AddPrimaryKey("dbo.Students", "Id");
            CreateIndex("dbo.Students", "UserId");
            CreateIndex("dbo.Students", "ClassId");
            AddForeignKey("dbo.Students", "ClassId", "dbo.Classes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Students", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            //DropTable("dbo.Persons");
        }
        
        public override void Down()
        {

            DropForeignKey("dbo.Students", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Students", "ClassId", "dbo.Classes");
            DropIndex("dbo.Students", new[] { "ClassId" });
            DropIndex("dbo.Students", new[] { "UserId" });
            DropPrimaryKey("dbo.Students");
            AlterColumn("dbo.Students", "StudentEmail", c => c.String());
            AlterColumn("dbo.Students", "StudentNumber", c => c.String());
            AlterColumn("dbo.Students", "Id", c => c.Int(nullable: false));
            DropColumn("dbo.Students", "ClassId");
            DropColumn("dbo.Students", "UserId");
            DropColumn("dbo.Students", "Deleted");
            DropColumn("dbo.Students", "Updated");
            DropColumn("dbo.Students", "Created");
            DropTable("dbo.Persons");
            AddPrimaryKey("dbo.Students", "Id");
            CreateIndex("dbo.Students", "Id");
            AddForeignKey("dbo.Students", "Id", "dbo.Persons", "Id");
        }
    }
}
