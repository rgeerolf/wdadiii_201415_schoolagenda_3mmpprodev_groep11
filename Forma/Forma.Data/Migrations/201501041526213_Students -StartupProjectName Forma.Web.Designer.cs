// <auto-generated />
namespace Forma.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class StudentsStartupProjectNameFormaWeb : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(StudentsStartupProjectNameFormaWeb));
        
        string IMigrationMetadata.Id
        {
            get { return "201501041526213_Students -StartupProjectName Forma.Web"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
