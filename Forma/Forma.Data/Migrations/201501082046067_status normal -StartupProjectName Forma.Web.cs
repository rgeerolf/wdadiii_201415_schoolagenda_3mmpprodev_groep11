namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class statusnormalStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ClassResponsibles", "Id", "dbo.Lecturers");
            DropIndex("dbo.ClassResponsibles", new[] { "Id" });
            DropTable("dbo.ClassResponsibles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ClassResponsibles",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ResponsibleNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ClassResponsibles", "Id");
            AddForeignKey("dbo.ClassResponsibles", "Id", "dbo.Lecturers", "Id");
        }
    }
}
