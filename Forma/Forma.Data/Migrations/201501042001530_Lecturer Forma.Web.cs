namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LecturerFormaWeb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lecturers", "Created", c => c.Binary());
            AddColumn("dbo.Lecturers", "Updated", c => c.DateTime());
            AddColumn("dbo.Lecturers", "Deleted", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lecturers", "Deleted");
            DropColumn("dbo.Lecturers", "Updated");
            DropColumn("dbo.Lecturers", "Created");
        }
    }
}
