namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Classes", "Responsible_Id", "dbo.ClassResponsibles");
            //DropForeignKey("dbo.Classes", "ResponsibleId", "dbo.ClassResponsibles");
            //DropIndex("dbo.Classes", new[] { "Responsible_Id" });
            ////DropColumn("dbo.Classes", "ResponsibleId");
            ////RenameColumn(table: "dbo.Classes", name: "Responsible_Id", newName: "ResponsibleId");
            //DropPrimaryKey("dbo.ClassResponsibles");
            ////AlterColumn("dbo.Classes", "ResponsibleId", c => c.Short(nullable: false));
            ////AlterColumn("dbo.ClassResponsibles", "Id", c => c.Short(nullable: false, identity: true));
            //AddPrimaryKey("dbo.ClassResponsibles", "Id");
            //CreateIndex("dbo.Classes", "ResponsibleId");
            //AddForeignKey("dbo.Classes", "ResponsibleId", "dbo.ClassResponsibles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classes", "ResponsibleId", "dbo.ClassResponsibles");
            DropIndex("dbo.Classes", new[] { "ResponsibleId" });
            DropPrimaryKey("dbo.ClassResponsibles");
            AlterColumn("dbo.ClassResponsibles", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Classes", "ResponsibleId", c => c.Int());
            AddPrimaryKey("dbo.ClassResponsibles", "Id");
            RenameColumn(table: "dbo.Classes", name: "ResponsibleId", newName: "Responsible_Id");
            AddColumn("dbo.Classes", "ResponsibleId", c => c.Short(nullable: false));
            CreateIndex("dbo.Classes", "Responsible_Id");
            AddForeignKey("dbo.Classes", "ResponsibleId", "dbo.ClassResponsibles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Classes", "Responsible_Id", "dbo.ClassResponsibles", "Id");
        }
    }
}
