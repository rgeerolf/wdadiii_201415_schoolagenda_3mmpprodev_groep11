// <auto-generated />
namespace Forma.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class PersonStartupProjectNameFormaWeb : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PersonStartupProjectNameFormaWeb));
        
        string IMigrationMetadata.Id
        {
            get { return "201501041353372_Person -StartupProjectName Forma.Web"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
