namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClassResponsibleStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClassResponsibles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResponsibleNumber = c.String(),
                        Created = c.Binary(),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        LecturerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .Index(t => t.LecturerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassResponsibles", "LecturerId", "dbo.Lecturers");
            DropIndex("dbo.ClassResponsibles", new[] { "LecturerId" });
            DropTable("dbo.ClassResponsibles");
        }
    }
}
