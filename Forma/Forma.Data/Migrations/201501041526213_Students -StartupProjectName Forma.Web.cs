namespace Forma.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentsStartupProjectNameFormaWeb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        StudentNumber = c.String(),
                        StudentEmail = c.String(),
                        StudentPicture = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Id)
                .Index(t => t.Id);
            
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.Persons", "StudentPicture", c => c.String());
            AddColumn("dbo.Persons", "StudentEmail", c => c.String());
            AddColumn("dbo.Persons", "StudentNumber", c => c.String());
            DropForeignKey("dbo.Students", "Id", "dbo.Persons");
            DropIndex("dbo.Students", new[] { "Id" });
            DropTable("dbo.Students");
        }
    }
}
