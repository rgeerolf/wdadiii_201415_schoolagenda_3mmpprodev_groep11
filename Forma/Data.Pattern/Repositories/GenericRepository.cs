﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Data.Pattern.Repositories
{
    public class GenericRepository<T> :
    IGenericRepository<T>
        where T : class
    {

        #region Private Fields
        private DbContext _context;
        private DbSet<T> _dbSet;
        #endregion

        #region Constructors
        public GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }
        #endregion

        public virtual IQueryable<T> GetAll()
        {

            IQueryable<T> query = _dbSet;
            return query;
        }

        public virtual T GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual async Task<T> GetByIdAsync(object id)
        {
            return await _dbSet.FindAsync(id);
        }


        public virtual IQueryable<T> GetBy(
            System.Linq.Expressions.Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            List<System.Linq.Expressions.Expression<Func<T, object>>> includes = null,
            int? page = null,
            int? pageSize = null)
        {
            IQueryable<T> query = _dbSet;

            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }
            return query;
        }

        public virtual async Task<IQueryable<T>> GetByAsync(
            System.Linq.Expressions.Expression<Func<T, bool>> query = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            List<System.Linq.Expressions.Expression<Func<T, object>>> includes = null,
            int? page = null,
            int? pageSize = null)
        {
            return await Task.Run(() => GetBy(query, orderBy, includes, page, pageSize)).ConfigureAwait(false);
        }

        public virtual void Insert(T entity)
        {
            _context.Entry(entity).State = EntityState.Added;
            _dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            var entity = _dbSet.Find(id);
            Delete(entity);
        }

        public virtual void Delete(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _context.Set<T>().Attach(entity);
            }
            _context.Entry(entity).State = EntityState.Deleted;
            _dbSet.Remove(entity);
        }

        public virtual async Task<bool> DeleteAsync(object id)
        {
            return await DeleteAsync(CancellationToken.None, id);
        }

        public virtual async Task<bool> DeleteAsync(CancellationToken cancellationToken, object id)
        {
            var entity = await _dbSet.FindAsync(cancellationToken, id);
            if (entity == null)
            {
                return false;
            }

            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _context.Set<T>().Attach(entity);
            }
            _context.Entry(entity).State = EntityState.Deleted;
            _dbSet.Remove(entity);

            return true;
        }

        public virtual void VirtualDelete(object id)
        {
            var entity = _dbSet.Find(id);
            VirtualDelete(entity);
        }

        public virtual void VirtualDelete(T entity)
        {
            if (entity.GetType().GetProperty("Deleted") != null)
            {
                entity.GetType().GetProperty("Deleted").SetValue(entity, DateTime.Now);
            }
            Update(entity);
        }

        public virtual void VirtualUnDelete(object id)
        {
            var entity = _dbSet.Find(id);
            VirtualUnDelete(entity);
        }

        public virtual void VirtualUnDelete(T entity)
        {
            if (entity.GetType().GetProperty("Deleted") != null)
            {
                entity.GetType().GetProperty("Deleted").SetValue(entity, null);
            }
            Update(entity);
        }

        public virtual void Update(T entity)
        {
            if (entity.GetType().GetProperty("Updated") != null)
            {
                entity.GetType().GetProperty("Updated").SetValue(entity, DateTime.Now);
            }
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _context.Set<T>().Attach(entity);
            }
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;

                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                }

            } while (saveFailed);
        }
    }
}
