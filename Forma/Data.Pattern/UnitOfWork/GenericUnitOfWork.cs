﻿using Data.Pattern.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Data.Pattern.UnitOfWork
{
    public class GenericUnitOfWork : IGenericUnitOfWork
    {
        #region Private Fields
        private readonly DbContext _context;
        private bool _disposed;
        private Dictionary<string, object> _repositories;
        private DbTransaction _transaction;
        #endregion Private Fields

        #region Constuctor/Dispose
        public GenericUnitOfWork(DbContext context) { _context = context; }

        public void Dispose()
        {
            if (_context != null && _context.Database.Connection.State == ConnectionState.Open)
            {
                _context.Database.Connection.Close();
            }

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }
        #endregion Constuctor/Dispose

        #region Repositories
        public IGenericRepository<T> Repository<T>() where T : class { return RepositoryAsync<T>(); }
        public IGenericRepository<T> RepositoryAsync<T>() where T : class
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (_repositories.ContainsKey(type))
            {
                return (IGenericRepository<T>)_repositories[type];
            }

            _repositories.Add(type, new GenericRepository<T>(_context));
            return (IGenericRepository<T>)_repositories[type];
        }
        #endregion

        #region SaveChanges
        public int SaveChanges() { return _context.SaveChanges(); }
        public Task<int> SaveChangesAsync() { return _context.SaveChangesAsync(); }
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken) { return _context.SaveChangesAsync(cancellationToken); }
        #endregion

        #region Unit of Work Transactions
        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            if (_context.Database.Connection.State != ConnectionState.Open)
            {
                _context.Database.Connection.Open();
            }

            _transaction = _context.Database.Connection.BeginTransaction();
        }

        public bool Commit()
        {
            _transaction.Commit();
            return true;
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }
        #endregion
    }
}
